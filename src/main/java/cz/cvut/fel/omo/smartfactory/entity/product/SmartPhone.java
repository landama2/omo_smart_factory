package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.LinkedList;

public class SmartPhone extends Product {

    public SmartPhone(int id) {
        this.id = id;
        this.requiredMaterials = new LinkedList<>();
        addRequiredMaterial();
    }


    private void addRequiredMaterial() {
        requiredMaterials.add(Material.CASE);
        requiredMaterials.add(Material.CAMERA);
        requiredMaterials.add(Material.BATTERY);
        requiredMaterials.add(Material.CHIP);
        requiredMaterials.add(Material.DISPLAY);
    }

    @Override
    public String getDescription() {
        return "Smartphone";
    }
}
