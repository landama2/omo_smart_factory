package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class HeadphonesAdder extends Machine {

    public HeadphonesAdder(int id) {
        super(Material.HEADPHONES, 1, 1, 2, id);
    }
}
