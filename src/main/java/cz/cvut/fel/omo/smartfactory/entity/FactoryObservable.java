package cz.cvut.fel.omo.smartfactory.entity;

public interface FactoryObservable {

    void addObserver(ElementObserver observer);
}
