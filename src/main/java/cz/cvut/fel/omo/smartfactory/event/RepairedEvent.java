package cz.cvut.fel.omo.smartfactory.event;

import cz.cvut.fel.omo.smartfactory.entity.employees.RepairMan;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class RepairedEvent extends Event {

    private RepairMan repairMan;
    private int brokenTime;

    public RepairedEvent(Machine source, RepairMan repairMan, int brokenTime) {
        super(source);
        this.repairMan = repairMan;
        this.brokenTime = brokenTime;
        handle(repairMan.toString());
    }

    public RepairMan getRepairMan() {
        return repairMan;
    }

    public int getBrokenTime() {
        return brokenTime;
    }

    public Machine getMachine() {
        return (Machine) source;
    }
}
