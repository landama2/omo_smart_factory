package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class CdRomAdder extends Machine {

    public CdRomAdder(int id) {
        super(Material.CD_ROM, 3, 2, 3, id);
    }
}
