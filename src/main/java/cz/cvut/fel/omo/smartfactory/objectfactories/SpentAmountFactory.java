package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.spendable.SpentAmount;

public class SpentAmountFactory {

    public static SpentAmount createSpentAmount(int amount) {
        return SpentAmount.builder()
                .amount(amount)
                .takt(SmartFactory.getCurrTakt())
                .build();
    }
}
