package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class ChipAdder extends Machine {

    public ChipAdder(int id) {
        super(Material.CHIP, 7, 1, 3, id);
    }
}
