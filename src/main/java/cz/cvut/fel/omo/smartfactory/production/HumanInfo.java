package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.spendable.Consumption;
import cz.cvut.fel.omo.smartfactory.spendable.Money;

public class HumanInfo extends Info {

//    private int moneyCost;
    private Money moneyCost = new Money();

    HumanInfo() {
    }

    private HumanInfo(HumanInfo info) {
        this.moneyCost = info.moneyCost;
    }

    @Override
    HumanInfo copy() {
        return new HumanInfo(this);
    }

    public Money getMoneyCost() {
        return moneyCost;
    }

    @Override
    void increment(Consumption consumption) {
//        moneyCost += consumption.getMoney();
        moneyCost.addAmount(consumption.getMoney());
    }

    @Override
    public String toString() {
        return "HumanInfo{" +
                "moneyCost=" + moneyCost +
                '}';
    }
}
