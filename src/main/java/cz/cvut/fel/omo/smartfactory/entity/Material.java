package cz.cvut.fel.omo.smartfactory.entity;

public enum Material {

    BUTTON(1),
    CAMERA(4),
    CHIP(5),
    BATTERY(5),
    DISPLAY(6),
    CASE(2),
    KEYBOARD(2),
    CD_ROM(3),
    HEADPHONES(4),
    CHARGER(1);

    private int cost;

    Material(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }
}
