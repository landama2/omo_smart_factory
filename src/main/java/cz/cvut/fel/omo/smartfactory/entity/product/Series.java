package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Visitable;
import cz.cvut.fel.omo.smartfactory.entity.Visitor;
import cz.cvut.fel.omo.smartfactory.objectfactories.ProductFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Series implements Visitable, SeriesIterable {

    private int id;
    private Product prototype;
    private List<Product> products = new ArrayList<>();
    private int amount = 0;

    public Series(Product prototype, int seriesSize) {
        this.prototype = prototype;
        for (int i = 0; i < seriesSize; i++) {
            this.products.add(ProductFactory.createSmartPhoneWithHeadPhonesForSeries());
        }
    }

    public int getAmount() {
        return amount;
    }

    public void completeProduct() {
        amount++;
    }

    public Product getProduct() {
        return this.prototype;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Iterator createIterator() {
        return products.iterator();
    }

    @Override
    public String toString() {
        return "Series{" +
                "id=" + id +
                ", prototype=" + prototype +
                ", productsInSeries=" + products.size() +
                ", finished products=" + amount +
                '}';
    }
}
