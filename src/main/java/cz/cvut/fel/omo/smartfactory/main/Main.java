package cz.cvut.fel.omo.smartfactory.main;

import cz.cvut.fel.omo.smartfactory.entity.Settings;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactoryController;

public class Main {
    public static void main(String[] args) {

        Settings settings = Settings.builder()
                .humans(5)
                .laptopProdLines(1)
                .phoneProdLines(1)
                .repairmen(5)
                .machines(5)
                .build();
        settings.storeToJson("settings.json");

        Settings settingsFromFile = Settings.loadFromJson("settings.json");

        SmartFactoryController controller = SmartFactoryController.getInstance();
        controller.createFactory(settingsFromFile);
        controller.simulate(2500);
        controller.printConsumptionReport();
        controller.printConsumptionReportInInterval(20, 50);
        controller.printEventsInInterval(20, 50);
        controller.printEvents();
        controller.printOutagesReportInInterval(20, 50);
        controller.printOutagesReport();
        controller.printAmountOfFinishedSeries();
        controller.clear();
    }
}
