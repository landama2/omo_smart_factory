package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.chain.BreakEventProcessor;
import cz.cvut.fel.omo.smartfactory.chain.Chain;
import cz.cvut.fel.omo.smartfactory.chain.CompleteEventProcessor;
import cz.cvut.fel.omo.smartfactory.chain.RepairEventProcessor;
import cz.cvut.fel.omo.smartfactory.entity.*;
import cz.cvut.fel.omo.smartfactory.entity.product.SmartPhone;
import cz.cvut.fel.omo.smartfactory.event.*;
import cz.cvut.fel.omo.smartfactory.objectfactories.ProductFactory;
import cz.cvut.fel.omo.smartfactory.entity.product.Laptop;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.entity.product.Series;

import java.util.LinkedList;
import java.util.List;

import static cz.cvut.fel.omo.smartfactory.objectfactories.SeriesFactory.SERIES_SIZE;

public class ProductionLine implements IterableItem, ElementObserver, Visitable {

    private final int MAX_AMOUNT = SERIES_SIZE;
    private List<ProductionLineElement> elements = new LinkedList<>();
    private int id;
    private ProductionLineElementPool prodPool = ProductionLineElementPool.getInstance();
    private Product product = null;
    private Series activeSeries = null;
    private int amount = 0;
    private boolean isOperational = false;
    private Chain eventProcessor;

    public ProductionLine(int id) {
        this.id = id;
        setupChain();
    }

    public int getId() {
        return id;
    }

    boolean assembleProductionLine(Product product) {
        elements.clear();
        Material material = product.getRequiredMaterials().poll();
        while (material != null) {
            List<ProductionLineElement> freeList = prodPool.getFreeList(material);
            if (freeList.size() > 0) {
                ProductionLineElement productionLineElement = freeList.get(0);
                elements.add(productionLineElement);
                prodPool.makeUnavailable(productionLineElement);
                if (productionLineElement instanceof Machine) {
                    ((Machine) productionLineElement).setPriority(id);
                }
            } else {
                System.out.println("Production line " + id + " could not be assembled!\n");
                for (ProductionLineElement element : elements) {
                    prodPool.makeAvailable(element);
                }
                return false;
            }
            material = product.getRequiredMaterials().poll();
        }
        System.out.println("Production line " + id + " was assembled.\n");

        this.product = product;
        this.activeSeries = new Series(product, SERIES_SIZE);
        this.isOperational = true;

        for (int i = 0; i < elements.size() - 1; i++) {
            elements.get(i).setNext(elements.get(i + 1));
        }

        for (int i = 0; i < elements.size() - 1; i++) {
            elements.get(i).setNext(elements.get(i + 1));
        }
        elements.forEach(el -> el.addObserver(this));
        SmartFactory.getInstance().addProductionLine(this);
        return true;
    }

    @Override
    public void iterate(IteratorConsumer consumer) {
        consumer.accept(this);
        elements.forEach(consumer::accept);
    }

    /**
     * Perform one takt
     */
    public void simulate() {
        Product product = null;
        if (this.isOperational) {
            if (amount < MAX_AMOUNT) {
                if (this.product instanceof Laptop) {
                    product = ProductFactory.createLaptop();
                } else if (this.product instanceof SmartPhone){
                    product = ProductFactory.createSmartPhone();
                }
            }
            if (product != null) {
                elements.get(0).acceptProduct(product);
            }
            for (int i = elements.size() - 1; i >= 0; i--) {
                elements.get(i).simulate();
            }
        }
    }

    @Override
    public void update(Event event) {
        eventProcessor.process(event);
    }

    private void setupChain() {
        Chain completeEventProcessor = new CompleteEventProcessor(this);
        Chain breakEventProcessor = new BreakEventProcessor(this);
        Chain repairEventProcessor = new RepairEventProcessor(this);

        completeEventProcessor.setNextChain(breakEventProcessor);
        breakEventProcessor.setNextChain(repairEventProcessor);
        this.eventProcessor = completeEventProcessor;
    }

    public Series getActiveSeries() {
        return this.activeSeries;
    }

    public List<ProductionLineElement> getElements() {
        return elements;
    }

    @Override
    public String toString() {
        return "ProductionLine{" +
                "id=" + id +
                '}';
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public boolean isOperational() {
        return isOperational;
    }

    public void setOperational(boolean operational) {
        isOperational = operational;
    }

    public void setActiveSeries(Series activeSeries) {
        this.activeSeries = activeSeries;
    }

    public Product getProduct() {
        return product;
    }
}
