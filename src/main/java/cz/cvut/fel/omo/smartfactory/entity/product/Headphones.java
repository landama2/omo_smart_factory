package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.Collections;

public class Headphones extends ProductDecorator implements Accessory {

    public Headphones(Product product) {
        super(product, Collections.singletonList(Material.HEADPHONES));
    }

    @Override
    public String getDescription() {
        return this.product.getDescription() + " and Headphones";
    }
}
