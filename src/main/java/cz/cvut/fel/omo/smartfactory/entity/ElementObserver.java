package cz.cvut.fel.omo.smartfactory.entity;

import cz.cvut.fel.omo.smartfactory.event.Event;

public interface ElementObserver {

    void update(Event event);
}
