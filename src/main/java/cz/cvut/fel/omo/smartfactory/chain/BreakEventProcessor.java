package cz.cvut.fel.omo.smartfactory.chain;

import cz.cvut.fel.omo.smartfactory.event.BreakEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLineElement;

public class BreakEventProcessor implements Chain {

    private  Chain nextInChain;
    private ProductionLine productionLine;

    public BreakEventProcessor(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public void setNextChain(Chain nextChain) {
        nextInChain = nextChain;
    }

    @Override
    public void process(Event event) {
        if (event instanceof BreakEvent) {
            notifyBreak((BreakEvent) event);
        } else {
            nextInChain.process(event);
        }
    }

    private void notifyBreak (BreakEvent event) {
        for (ProductionLineElement productionLineElement : productionLine.getElements()) {
            if (productionLineElement == event.getMachine()) {
                productionLine.setOperational(false);
                break;
            }
        }
    }
}