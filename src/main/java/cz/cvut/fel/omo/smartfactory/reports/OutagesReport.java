package cz.cvut.fel.omo.smartfactory.reports;

import cz.cvut.fel.omo.smartfactory.entity.IterableItem;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.production.Machine;

import java.util.*;
import java.util.stream.Collectors;

public class OutagesReport extends Reporter implements IteratorConsumer {

    private long outageCount;
    private int maxOutageTime = 0;
    private int minOutageTime = Integer.MAX_VALUE;
    private int totalOutageTime = 0;

    private Map<Machine, Integer> machines = new HashMap<>();

    public OutagesReport() {
        super("outages_report.txt");
    }

    public void generateReport(SmartFactory factory) {
        factory.iterate(this);
    }

    @Override
    public void accept(IterableItem item) {
        if (item instanceof Machine) {
            handleMachine((Machine) item);
        }
    }

    private void handleMachine(Machine machine) {
        if (machine.getOutages().size() == 0) {
            return;
        }

        int min = machine.getOutages()
                .stream()
                .map(Outage::getLenght)
                .min(Comparator.naturalOrder())
                .orElse(0);
        int max = machine.getOutages()
                .stream()
                .map(Outage::getLenght)
                .max(Comparator.naturalOrder())
                .orElse(0);
        minOutageTime = Math.min(min, minOutageTime);
        maxOutageTime = Math.max(max, maxOutageTime);

        Integer outageTime = machine.getOutages()
                .stream()
                .map(Outage::getLenght)
                .reduce(0, (a, b) -> a + b);
        totalOutageTime += outageTime;

        outageCount += machine.getOutages()
                .stream()
                .map(Outage::getLenght)
                .count();

        machines.put(machine, outageTime);
    }

    public void printReport() {

        System.out.println();
        System.out.println("====== Outages report ======");
        System.out.println();
        System.out.println("Min outage length: " + minOutageTime);
        System.out.println("Max outage length: " + maxOutageTime);
        double average = outageCount == 0 ? 0 : (double) totalOutageTime / (double) outageCount;
        System.out.println("average outage length: " + average);
        System.out.println("Outage count: " + outageCount);
        System.out.println("Total outage time: " + totalOutageTime);
        System.out.println();

        List<Map.Entry<Machine, Integer>> sorted = sortByValue(machines);
        sorted.forEach(entry -> System.out.println(entry.getKey() + ": outage time: " + entry.getValue()));
    }

    private List<Map.Entry<Machine, Integer>> sortByValue(Map<Machine, Integer> machines) {
        return machines.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .collect(Collectors.toList());
    }

    @Override
    public String createReport() {
        List<String> lines = new ArrayList<>();
        lines.add("====== Outages report ======");
        lines.add("Min outage length: " + minOutageTime);
        lines.add("Max outage length: " + maxOutageTime);
        lines.add("average outage length: " + (outageCount == 0 ? 0 : (double) totalOutageTime / (double) outageCount));
        lines.add("Outage count: " + outageCount);
        lines.add("Total outage time: " + totalOutageTime);

        List<Map.Entry<Machine, Integer>> sorted = sortByValue(machines);
        sorted.forEach(entry -> lines.add(entry.getKey() + ": outage time: " + entry.getValue()));
        return String.join("\n", lines);
    }

    @Override
    public String createReportInRange(int from, int to) {
        List<String> lines = new ArrayList<>();
        if (coversAll(from, to)) {
            lines.add("====== Outages report ("+ SmartFactory.getCurrTakt()+" taktss) ======");
        } else {
            lines.add("====== Outages report (" + from + " -> " + to + ") ======");
        }
        lines.add("Min outage length: " + minOutageTime);
        lines.add("Max outage length: " + maxOutageTime);
        lines.add("average outage length: " + (outageCount == 0 ? 0 : (double) totalOutageTime / (double) outageCount));
        lines.add("Outage count: " + outageCount);
        lines.add("Total outage time: " + totalOutageTime);

        List<Map.Entry<Machine, Integer>> sorted = sortByValue(machines);
        sorted.forEach(entry -> lines.add(entry.getKey() + ": outage time: " + entry.getValue()));
        return String.join("\n", lines);
    }
}
