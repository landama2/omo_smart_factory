package cz.cvut.fel.omo.smartfactory.spendable.quality;

public class WearLevel implements Breakable, Comparable<WearLevel> {

    protected int damageAmount = 0;
    private int damageLimit;
    private boolean broken = false;

    public WearLevel(int damageLimit) {
        this.damageLimit = damageLimit;
    }

    @Override
    public int getDamageAmount() {
        return damageAmount;
    }

    @Override
    public void damage(int amount) {
        this.damageAmount += amount;
        if (this.damageAmount >= damageLimit) {
            broken = true;
        }

    }

    public void repairDamage(int amountRepaired) {
        damageAmount = Math.max(0, damageAmount - amountRepaired);
        if (damageAmount == 0) {
            broken = false;
        }
    }

    public boolean isBroken() {
        return broken;
    }

    @Override
    public int compareTo(WearLevel o) {
        return damageAmount - o.getDamageAmount();
    }

    @Override
    public String toString() {
        return "WearLevel{" +
                "damageAmount=" + damageAmount +
                ", damageLimit=" + damageLimit +
                ", broken=" + isBroken() +
                '}';
    }
}
