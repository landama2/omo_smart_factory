package cz.cvut.fel.omo.smartfactory.reports;

import cz.cvut.fel.omo.smartfactory.entity.FactoryObservable;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.event.Event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventReport extends Reporter {

    private static EventReport instance;
    private List<Event> events = new ArrayList<>();

    private EventReport() {
        super("events_report.txt");
    }

    public static EventReport getInstance() {
        if (instance == null) {
            instance = new EventReport();
        }
        return instance;
    }

    public void clear() {
        events = new ArrayList<>();
    }

    public void registerEvent(Event event) {
        events.add(event);
    }

    public void printEvents() {
        Map<Class<? extends Event>, Integer> types = eventsByType();
        System.out.println("====== Event report ======");
        System.out.println();
        types.forEach((event, count) -> System.out.println(event.getSimpleName() + ": " + count + "x"));
        Map<FactoryObservable, Integer> eventsBySource = eventsBySource();
        eventsBySource.forEach((source, count) -> System.out.println(source + ": " + count + "x"));
        Map<String, Integer> eventsByHandler = eventsByHandler();
        eventsByHandler.forEach((handler, count) -> System.out.println(handler + ": " + count + "x"));
    }

    private Map<Class<? extends Event>, Integer> eventsByType(int from, int to) {
        Map<Class<? extends Event>, Integer> typeToCount = new HashMap<>();

        events.stream()
                .filter(event -> event.getTakt() >= from && event.getTakt() <= to)
                .forEach(e -> {
                    Class<? extends Event> clazz = e.getClass();
                    typeToCount.putIfAbsent(clazz, 0);
                    Integer count = typeToCount.get(clazz);
                    typeToCount.put(clazz, count + 1);
                });
        return typeToCount;
    }

    private Map<FactoryObservable, Integer> eventsBySource(int from, int to) {
        Map<FactoryObservable, Integer> typeToCount = new HashMap<>();

        events.stream()
                .filter(event -> event.getTakt() >= from && event.getTakt() <= to)
                .forEach(e -> {
                    FactoryObservable source = e.getSource();
                    typeToCount.putIfAbsent(source, 0);
                    Integer count = typeToCount.get(source);
                    typeToCount.put(source, count + 1);
                });
        return typeToCount;
    }

    private Map<String, Integer> eventsByHandler(int from, int to) {
        Map<String, Integer> sourceToCount = new HashMap<>();

        events.stream()
                .filter(event -> event.getTakt() >= from && event.getTakt() <= to)
                .forEach(e -> {
                    String handler = e.getHandler();
                    sourceToCount.putIfAbsent(handler, 0);
                    Integer count = sourceToCount.get(handler);
                    sourceToCount.put(handler, count + 1);
                });
        return sourceToCount;
    }

    @Override
    public String createReport() {
        List<String> lines = new ArrayList<>();
        Map<Class<? extends Event>, Integer> types = eventsByType();
        lines.add("====== Event report ("+ SmartFactory.getCurrTakt()+" takts) ======");
        types.forEach((event, count) -> lines.add(event.getSimpleName() + ": " + count + "x"));
        Map<FactoryObservable, Integer> eventsBySource = eventsBySource();
        eventsBySource.forEach((source, count) -> lines.add(source + ": " + count + "x"));
        Map<String, Integer> eventsByHandler = eventsByHandler();
        eventsByHandler.forEach((handler, count) -> lines.add(handler + ": " + count + "x"));
        return String.join("\n", lines);
    }

    private Map<String, Integer> eventsByHandler() {
        return eventsByHandler(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private Map<FactoryObservable, Integer> eventsBySource() {
        return eventsBySource(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private Map<Class<? extends Event>, Integer> eventsByType() {
        return eventsByType(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public String createReportInRange(int from, int to) {
        List<String> lines = new ArrayList<>();
        if (coversAll(from, to)) {
            lines.add("====== Event report ("+ SmartFactory.getCurrTakt()+" taktss) ======");
        } else {
            lines.add("====== Event report (" + from + " -> " + to + ") ======");
        }
        Map<Class<? extends Event>, Integer> types = eventsByType(from, to);

        types.forEach((event, count) -> lines.add(event.getSimpleName() + ": " + count + "x"));
        Map<FactoryObservable, Integer> eventsBySource = eventsBySource(from, to);
        eventsBySource.forEach((source, count) -> lines.add(source + ": " + count + "x"));
        Map<String, Integer> eventsByHandler = eventsByHandler(from, to);
        eventsByHandler.forEach((handler, count) -> lines.add(handler + ": " + count + "x"));
        return String.join("\n", lines);
    }
}
