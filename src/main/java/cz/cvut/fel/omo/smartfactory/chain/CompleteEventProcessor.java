package cz.cvut.fel.omo.smartfactory.chain;

import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactoryController;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.entity.product.Series;
import cz.cvut.fel.omo.smartfactory.event.CompleteEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLineElement;

import static cz.cvut.fel.omo.smartfactory.objectfactories.SeriesFactory.SERIES_SIZE;

public class CompleteEventProcessor implements Chain {

    private Chain nextInChain;
    private ProductionLine productionLine;

    public CompleteEventProcessor(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public void setNextChain(Chain nextChain) {
        nextInChain = nextChain;
    }

    @Override
    public void process(Event event) {
        if (event instanceof CompleteEvent) {
            notifyCompletion((CompleteEvent) event);
        } else {
            nextInChain.process(event);
        }
    }

    private void notifyCompletion(CompleteEvent event) {
        for (ProductionLineElement productionLineElement : productionLine.getElements()) {
            if (event.getElement() == productionLineElement) {
                Product product = event.getElement().workOnProduct();
                if (product != null) {
                    if (product.getRequiredMaterials().poll() == null) {
                        Series activeSeries = productionLine.getActiveSeries();
                        activeSeries.completeProduct();
                        if (activeSeries.getAmount() == SERIES_SIZE) {
                            SmartFactory.getInstance().addFinishedProducts(activeSeries);
                            productionLine.setActiveSeries(new Series(SmartFactoryController.getInstance().getNextProduct(), SERIES_SIZE));
                        }
                    }
                }
            }
        }
    }
}
