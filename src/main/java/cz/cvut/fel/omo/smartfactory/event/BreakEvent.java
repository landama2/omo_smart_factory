package cz.cvut.fel.omo.smartfactory.event;

import cz.cvut.fel.omo.smartfactory.production.Machine;

public class BreakEvent extends Event {

    public BreakEvent(Machine source) {
        super(source);
    }

    public Machine getMachine() {
        return (Machine) source;
    }
}
