package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.Collections;

public class Charger extends ProductDecorator implements Accessory {
    public Charger(Product product) {
        super(product, Collections.singletonList(Material.CHARGER));
    }

    @Override
    public String getDescription() {
        return this.product.getDescription() + " and Charger";
    }
}
