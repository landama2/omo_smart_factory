package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.production.machines.*;

public class MachineFactory {

    private static int machineId = 0;

    public static BatteryAdder createBatteryAdder() {
        return new BatteryAdder(machineId++);
    }

    public static CameraAdder createCameraAdder() {
        return new CameraAdder(machineId++);
    }

    public static ButtonAdder createButtonAdder() {
        return new ButtonAdder(machineId++);
    }

    public static CaseAdder createCaseAdder() {
        return new CaseAdder(machineId++);
    }

    public static CdRomAdder createCdRomAdder() {
        return new CdRomAdder(machineId++);
    }

    public static ChargerAdder createChargerAdder() {
        return new ChargerAdder(machineId++);
    }

    public static ChipAdder createChipAdder() {
        return new ChipAdder(machineId++);
    }

    public static DisplayAdder createDisplayAdder() {
        return new DisplayAdder(machineId++);
    }

    public static HeadphonesAdder createHeadphonesAdder() {
        return new HeadphonesAdder(machineId++);
    }

    public static KeyboardAdder createKeyboardAdder() {
        return new KeyboardAdder(machineId++);
    }

}
