package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class BatteryAdder extends Machine {

    public BatteryAdder(int id) {
        super(Material.BATTERY, 2, 1, 2, id);
    }
}
