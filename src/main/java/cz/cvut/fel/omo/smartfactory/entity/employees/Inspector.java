package cz.cvut.fel.omo.smartfactory.entity.employees;

import cz.cvut.fel.omo.smartfactory.entity.IterableItem;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class Inspector implements IteratorConsumer {

    @Override
    public void accept(IterableItem item) {
        if (!(item instanceof Machine)) {
            return;
        }

        System.out.println("Inspector visits: " + item);
    }

}
