package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.entity.ElementObserver;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;

public class Robot implements ProductionLineElement {
    @Override
    public Material getProvidedMaterial() {
        return null;
    }

    @Override
    public void giveMaterial(Material material) {

    }

    @Override
    public Info getInfo() {
        return null;
    }

    @Override
    public void acceptProduct(Product product) {

    }

    @Override
    public void setNext(ProductionLineElement next) {

    }

    @Override
    public void simulate() {

    }

    @Override
    public Product workOnProduct() {
        return null;
    }

    @Override
    public void addObserver(ElementObserver observer) {

    }

    @Override
    public void iterate(IteratorConsumer consumer) {

    }
}
