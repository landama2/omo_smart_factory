package cz.cvut.fel.omo.smartfactory.entity.employees;

import cz.cvut.fel.omo.smartfactory.entity.IterableItem;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.entity.Visitor;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.entity.product.Series;
import cz.cvut.fel.omo.smartfactory.production.Machine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;


public class Director implements IteratorConsumer, Visitor {

    private static Logger logger = LoggerFactory.getLogger(Director.class);

    @Override
    public void accept(IterableItem item) {
        logger.info("Director visits: {}", item);
    }

    @Override
    public void visit(Machine machine) {
        logger.info("Director visits: {}", machine);
        if (machine.isBroken()) {
            logger.warn("Machine {} is broken. The Director is not pleased.");
        }
        machine.iterate(this);
    }

    @Override
    public void visit(SmartFactory factory) {
        logger.info("Director visits: {} on takts {}", factory, factory.getCurrentTakt());
//        factory.getAmountOfFinishedLaptopSeries()

        for (ProductionLine productionLine : factory.getProductionLines()) {
            productionLine.accept(this);
        }
//        machine.iterate(this);

    }

    @Override
    public void visit(ProductionLine line) {
        logger.info("Director visits: {}", line);

        Series series = line.getActiveSeries();
//        line.getElements().forEach(productionLineElement -> productionLineElement);
        series.accept(this);
    }

    @Override
    public void visit(Series series) {
        logger.info("Director visits: {}", series);
        Iterator iterator = series.createIterator();
        while (iterator.hasNext()) {
            Product product = (Product) iterator.next();
            logger.info("Director checks on Product {}", product);
            if (product.isCompleted()) {
                logger.info("Director is happy.");
            }
        }
    }
}
