package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.List;

public abstract class ProductDecorator extends Product {

    protected Product product;

    public ProductDecorator(Product product, List<Material> extraMaterial) {
        this.product = product;
        this.requiredMaterials = product.requiredMaterials;
        this.requiredMaterials.addAll(extraMaterial);
    }

}
