package cz.cvut.fel.omo.smartfactory.spendable;

import java.util.List;
import java.util.Set;

public interface Spendable {

    int getAmount();

    void addAmount(int amount);

    void addAmount(Spendable spendable);

    Set<SpentAmount> getSpentAmounts();
}
