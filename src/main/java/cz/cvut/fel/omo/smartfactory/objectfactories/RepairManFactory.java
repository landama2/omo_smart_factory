package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.entity.employees.RepairMan;

public class RepairManFactory {

    private static int globalId = 0;
    private static RepairManFactory instance;

    public static RepairManFactory getInstance() {
        if (instance == null) {
            instance = new RepairManFactory();
        }
        return instance;
    }
    public static RepairMan createRepairMan() {
        return new RepairMan(globalId++);
    }
}
