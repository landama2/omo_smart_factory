package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class DisplayAdder extends Machine {

    public DisplayAdder(int id) {
        super(Material.DISPLAY, 5, 3, 4, id);
    }
}
