package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.objectfactories.ProductionLineFactory;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;

public class ProductionLineManagement {
    private static ProductionLineManagement instance;

    private ProductionLineManagement() {}

    public static ProductionLineManagement getInstance() {
        if(instance == null) {
            instance = new ProductionLineManagement();
        }
        return instance;
    }

    public boolean assembleProductionLine(Product product) {
        ProductionLine productionLine = ProductionLineFactory.createProductionLine();
        return productionLine.assembleProductionLine(product);
    }
}
