package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.entity.ElementObserver;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.event.CompleteEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.spendable.Consumption;

import java.util.HashSet;
import java.util.Set;

public abstract class ProductionLineElementImpl implements ProductionLineElement {

    int id;
    Material providedMaterial;
    Info info;
    private Product product;
    Consumption consumption;
    private ProductionLineElement next;
    Set<ElementObserver> observers = new HashSet<>();

    @Override
    public Material getProvidedMaterial() {
        return providedMaterial;
    }

    @Override
    public void giveMaterial(Material material) {
        this.providedMaterial = material;
    }

    @Override
    public Info getInfo() {
        return info.copy();
    }

    @Override
    public void acceptProduct(Product product) {
        this.product = null;
        Material material = product.getRequiredMaterials().poll();
        if ((material != null) && (material == this.providedMaterial)) {
            info.increment(consumption);
            product.workOn();
        }
        this.product = product;
    }

    @Override
    public void setNext(ProductionLineElement next) {
        this.next = next;
    }

    @Override
    public void simulate() {
        if (this.product != null) {
            if (this.next != null) {
                this.next.acceptProduct(product);
            } else {
                notifyObservers(new CompleteEvent(this));
            }
        }
    }

    private void notifyObservers(Event event) {
        observers.forEach(observers -> observers.update(event));
    }

    @Override
    public Product workOnProduct() {
        Product toRet = product;
        product.workOn();
        this.product = null;
        return toRet;
    }

    @Override
    public void addObserver(ElementObserver observer) {
        observers.add(observer);
    }

    @Override
    public void iterate(IteratorConsumer consumer) {
        consumer.accept(this);
    }

    public Consumption getConsumption() {
        return consumption;
    }
}
