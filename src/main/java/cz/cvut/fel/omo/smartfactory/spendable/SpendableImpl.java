package cz.cvut.fel.omo.smartfactory.spendable;

import cz.cvut.fel.omo.smartfactory.objectfactories.SpentAmountFactory;

import java.util.HashSet;
import java.util.Set;

public class SpendableImpl implements Spendable {

    private Set<SpentAmount> spentAmounts = new HashSet<>();

    @Override
    public int getAmount() {
        return spentAmounts.stream()
                .map(SpentAmount::getAmount)
                .reduce(0, (a, b) ->  a + b );
    }

    /**
     * @param from takt
     * @param to takt
     * @return amount between takts
     */
    public int getAmountInRange(int from, int to) {
        return spentAmounts.stream()
                .filter(spentAmount -> spentAmount.getTakt() >= from && spentAmount.getTakt() <= to)
                .map(SpentAmount::getAmount)
                .reduce(0, (a, b) ->  a + b );
    }

    /**
     * Adds amount
     * @param amount amount
     */
    @Override
    public void addAmount(int amount) {
        spentAmounts.add(SpentAmountFactory.createSpentAmount(amount));
    }

    /** Adds amount from another spendable
     * @param spendable spendable object
     */
    @Override
    public void addAmount(Spendable spendable) {
        spentAmounts.addAll(spendable.getSpentAmounts());
    }

    public Set<SpentAmount> getSpentAmounts() {
        return spentAmounts;
    }
}
