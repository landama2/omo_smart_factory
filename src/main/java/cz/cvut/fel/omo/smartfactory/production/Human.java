package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.spendable.Consumption;

public class Human extends ProductionLineElementImpl {

    public Human(Material material, int cost, int id) {
        this.providedMaterial = material;
        this.consumption = Consumption.builder()
            .money(cost)
            .build();
        this.id = id;
        this.info = new HumanInfo();
    }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                '}';
    }
}
