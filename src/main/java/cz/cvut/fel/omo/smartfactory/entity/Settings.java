package cz.cvut.fel.omo.smartfactory.entity;

import com.google.gson.Gson;
import lombok.Builder;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Objects;

@Builder
@Getter
public class Settings {

    private int humans;
    int machines;
    int phoneProdLines;
    int laptopProdLines;
    int repairmen;

    private static Logger logger = LoggerFactory.getLogger(Settings.class);

    public void storeToJson(String filename) {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        try (BufferedWriter writer =
                     new BufferedWriter(new FileWriter(filename))) {
            writer.append(json);
        } catch (IOException e) {
            logger.error("Failed to append to file: " + filename, e);
        }
    }

    public static Settings loadFromJson(String filename) {
        Gson gson = new Gson();
        try (FileReader fileReader = new FileReader(filename)) {
            Settings settings = gson.fromJson(fileReader, Settings.class);
            return settings;
        } catch (IOException e) {
            logger.error("Failed to append to file: " + filename, e);
        }
        return Settings.builder().build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Settings settings = (Settings) o;
        return humans == settings.humans &&
                machines == settings.machines &&
                phoneProdLines == settings.phoneProdLines &&
                laptopProdLines == settings.laptopProdLines &&
                repairmen == settings.repairmen;
    }

    @Override
    public int hashCode() {
        return Objects.hash(humans, machines, phoneProdLines, laptopProdLines, repairmen);
    }
}
