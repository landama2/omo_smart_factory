package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.entity.product.Headphones;
import cz.cvut.fel.omo.smartfactory.entity.product.Laptop;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.entity.product.SmartPhone;

public class ProductFactory {

    private static int productId = 0;

    public static Laptop createLaptop() {
        return new Laptop(productId++);
    }

    public static SmartPhone createSmartPhone() {
        return new SmartPhone(productId++);
    }

    public static Headphones createSmartPhoneWithHeadPhonesForSeries() {
        SmartPhone smartPhone = new SmartPhone(productId++);
        smartPhone.plan();
        Headphones headphones = new Headphones(smartPhone);
        headphones.plan();
        return headphones;
    }

    public static SmartPhone createSmartPhoneForSeries() {
        SmartPhone smartPhone = new SmartPhone(productId++);
        smartPhone.plan();
        return smartPhone;
    }

    public static Laptop createLaptopForSeries() {
        Laptop laptop = new Laptop(productId++);
        laptop.plan();
        return laptop;
    }
//
//    public static Product createProduct(Product product) {
//        return new (productId++);
//    }
}
