package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.spendable.Consumption;
import cz.cvut.fel.omo.smartfactory.spendable.Energy;
import cz.cvut.fel.omo.smartfactory.spendable.Oil;
import cz.cvut.fel.omo.smartfactory.spendable.quality.WearLevel;

public class MachineInfo extends Info {

    private Energy totalEnergyConsumption = new Energy();
    private Oil totalOilConsumption = new Oil();
    private Machine machine;

    public MachineInfo(Machine machine) {
        this.machine = machine;
    }

    public MachineInfo(MachineInfo info) {
        this.totalEnergyConsumption = info.totalEnergyConsumption;
        this.totalOilConsumption = info.totalOilConsumption;
        this.machine = info.machine;
    }

    @Override
    public MachineInfo copy() {
        return new MachineInfo(this);
    }

    public Energy getTotalEnergyConsumption() {
        return totalEnergyConsumption;
    }

    public Oil getTotalOilConsumption() {
        return totalOilConsumption;
    }

    public WearLevel getWearLevel() {
        return machine.getWearLevel();
    }

    @Override
    public String toString() {
        return "Machine: total energy consumption = " + totalEnergyConsumption + ", total oil consumption = " + totalOilConsumption + ", wear level = " + getWearLevel();
    }


    @Override
    void increment(Consumption consumption) {
        if (machine.use(consumption.getWearFactor())) {
            totalEnergyConsumption.addAmount(consumption.getEnergyConsumption());
            totalOilConsumption.addAmount(consumption.getOilConsumption());
        }
    }

}
