package cz.cvut.fel.omo.smartfactory.entity.pools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ObjectPool<T> {

    protected Map<T, Boolean> objects = new HashMap<>();

    /**
     * Returns stream of free objects
     * without marking them as used
     *
     * @return stream of free objects
     */
    public Stream<T> availableStream() {
        return objects.entrySet()
                .stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey);
    }

    /**
     * Returns list of free objects
     * without marking them as used
     *
     * @return list of free objects
     */
    public List<T> peekAvailableList() {
        return this.availableStream()
                .collect(Collectors.toList());
    }

    /**
     * Returns free object
     * without marking it as used
     *
     * @return free object or null
     */
    private T peekAvailable() {
        return objects.entrySet()
                .stream()
                .filter(Map.Entry::getValue)
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(null);
    }

    /**
     * Marks object as used and returns it
     *
     * @return free object or null
     */
    public T pollAvailable() {
        T object = peekAvailable();
        if (object == null) {
            return null;
        }
        objects.put(object, false);
        return object;
    }

    /**
     * Sets object free
     *
     * @param object used or new object
     */
    public void makeAvailable(T object) {
        objects.put(object, true);
    }

    /**
     * Sets object taken
     *
     * @param object used or new object
     */
    public void makeUnavailable(T object) {
        objects.put(object, false);
    }
}
