package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class CameraAdder extends Machine {

    public CameraAdder(int id) {
        super(Material.CAMERA, 5, 3, 4, id);
    }
}
