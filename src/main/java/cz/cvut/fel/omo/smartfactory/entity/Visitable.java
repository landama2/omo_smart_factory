package cz.cvut.fel.omo.smartfactory.entity;

public interface Visitable {
    public void accept(Visitor visitor);
}
