package cz.cvut.fel.omo.smartfactory.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public abstract class Reporter {

    private static Logger logger = LoggerFactory.getLogger(ConsumptionReport.class);
    private String filename;

    Reporter(String filename) {
        this.filename = filename;
    }

    private void printReportToFile(String filename) {
        printReportToFileInRange(filename, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private void printReportToFileInRange(String filename, int from, int to) {
        String string = createReportInRange(from, to);
        try (BufferedWriter writer =
                     new BufferedWriter(new FileWriter(filename, true))) {
            writer.append('\n');
            writer.append(string);
        } catch (IOException e) {
            logger.error("Failed to append to file: " + filename, e);
        }
    }

    public void printReportToSout() {
        String report = createReport();
        System.out.println(report);
    }

    public void printReportToFile() {
        printReportToFile(filename);
    }

    public void printReportToFileInRange(int from, int to) {
        printReportToFileInRange(filename, from, to);
    }

    public abstract String createReport();

    public abstract String createReportInRange(int from, int to);

    boolean coversAll(int from, int to) {
        return from == Integer.MIN_VALUE && to == Integer.MAX_VALUE;
    }

}
