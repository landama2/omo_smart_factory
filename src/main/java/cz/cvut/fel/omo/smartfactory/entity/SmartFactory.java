package cz.cvut.fel.omo.smartfactory.entity;

import cz.cvut.fel.omo.smartfactory.entity.pools.RepairManPool;
import cz.cvut.fel.omo.smartfactory.entity.product.Headphones;
import cz.cvut.fel.omo.smartfactory.entity.product.Laptop;
import cz.cvut.fel.omo.smartfactory.entity.product.SmartPhone;
import cz.cvut.fel.omo.smartfactory.entity.product.Series;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;

import java.util.ArrayList;
import java.util.List;

public class SmartFactory implements IterableItem, Visitable {

    private static SmartFactory instance = null;
    private List<ProductionLine> productionLines = new ArrayList<>();
    private RepairManPool repairManPool = RepairManPool.getInstance();
    private int currentTakt = 0;
    private List<Series> finishedProducts = new ArrayList<>();

    private SmartFactory() {

    }

    public static SmartFactory getInstance() {
        if (instance == null) {
            instance = new SmartFactory();
        }
        return instance;
    }

    public List<ProductionLine> getProductionLines() {
        return productionLines;
    }

    public List<ProductionLine> getPhoneProductionLines() {
        List<ProductionLine> toRet = new ArrayList<>();
        for(ProductionLine productionLine : productionLines) {
            if(productionLine.getActiveSeries().getProduct() instanceof SmartPhone) {
                toRet.add(productionLine);
            }
        }
        return toRet;
    }

    public List<ProductionLine> getLaptopProductionLines() {
        List<ProductionLine> toRet = new ArrayList<>();
        for(ProductionLine productionLine : productionLines) {
            if(productionLine.getActiveSeries().getProduct() instanceof Laptop) {
                toRet.add(productionLine);
            }
        }
        return toRet;
    }

    public void clear() {
        finishedProducts = new ArrayList<>();
        productionLines = new ArrayList<>();
        repairManPool.clear();
    }

    @Override
    public void iterate(IteratorConsumer consumer) {
        consumer.accept(this);
        productionLines.forEach(line -> line.iterate(consumer));
    }

    /**
     * @param visitor
     */
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    /**
     * @param visitor
     */
    public void acceptDirector(Visitor visitor) {
        accept(visitor);
    }

    /**
     * Adds production line.
     * @param productionLine
     */
    public void addProductionLine(ProductionLine productionLine) {
        if (productionLines == null) {
            productionLines = new ArrayList<>();
        }
        productionLines.add(productionLine);
    }

    protected void simulate() {
        currentTakt++;
        for (ProductionLine pl : productionLines) {
            pl.simulate();
        }
        repairManPool.simulate();
    }

    public void addFinishedProducts(Series serie) {
        finishedProducts.add(serie);
    }

    int getAmountOfFinishedSeries() {
        return finishedProducts.size();
    }

    int getAmountOfFinishedPhoneSeries() {
        int amount = 0;
        for (Series s : finishedProducts) {
            if (s.getProduct() instanceof SmartPhone) {
                amount++;
            }
        }
        return amount;
    }

    int getAmountOfFinishedPhoneWithHpSeries() {
        int amount = 0;
        for (Series s : finishedProducts) {
            if (s.getProduct() instanceof Headphones) {
                amount++;
            }
        }
        return amount;
    }

    int getAmountOfFinishedLaptopSeries() {
        int amount = 0;
        for (Series s : finishedProducts) {
            if (s.getProduct() instanceof Laptop) {
                amount++;
            }
        }
        return amount;
    }

    public int getCurrentTakt() {
        return currentTakt;
    }

    /**
     * @return Number of last takts(takt) of factory.
     */
    public static int getCurrTakt(){
        return instance != null ? instance.currentTakt : 0;
    }

    @Override
    public String toString() {
        return "SmartFactory{}";
    }

}
