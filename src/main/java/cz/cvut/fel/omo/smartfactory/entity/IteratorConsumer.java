package cz.cvut.fel.omo.smartfactory.entity;

import java.util.function.Consumer;

public interface IteratorConsumer extends Consumer<IterableItem> {

    void accept(IterableItem item);
}
