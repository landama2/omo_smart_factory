package cz.cvut.fel.omo.smartfactory.chain;

import cz.cvut.fel.omo.smartfactory.event.Event;

public interface Chain {

    void setNextChain(Chain nextChain);

    void process(Event event);

}
