package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.entity.Visitable;
import cz.cvut.fel.omo.smartfactory.entity.Visitor;
import cz.cvut.fel.omo.smartfactory.entity.employees.RepairMan;
import cz.cvut.fel.omo.smartfactory.event.BreakEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.event.RepairedEvent;
import cz.cvut.fel.omo.smartfactory.reports.Outage;
import cz.cvut.fel.omo.smartfactory.spendable.Consumption;
import cz.cvut.fel.omo.smartfactory.spendable.quality.WearLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public abstract class Machine extends ProductionLineElementImpl implements Visitable {

    private static Logger logger = LoggerFactory.getLogger(Machine.class);

    private static final int WEAR_LIMIT = 100;

    private int id;
    private int priority;
    private WearLevel wearLevel = new WearLevel(WEAR_LIMIT);

    private int brokenAt;
    private Set<Outage> outages = new HashSet<>();

    public Machine(Material material, int energyConsumption, int oilConsumption, int wearFactor, int id) {
        this.providedMaterial = material;
        consumption = Consumption.builder()
                .energyConsumption(energyConsumption)
                .oilConsumption(oilConsumption)
                .wearFactor(wearFactor)
                .build();
        this.id = id;
        this.info = new MachineInfo(this);
    }

    public int getId() {
        return this.id;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    private void notifyObservers(Event event) {
        observers.forEach(observers -> observers.update(event));
    }

    boolean use(int damage) {
        if (this.wearLevel.isBroken()) {
            return false;//can't use!!
        }
        this.wearLevel.damage(damage);
        if (this.wearLevel.isBroken()) {//is broken after usage
            breakMachine();
        }
        return true;
    }

    private void breakMachine() {
        brokenAt = SmartFactory.getInstance().getCurrentTakt();

        logger.info("{} is broken", this);
        notifyObservers(new BreakEvent(this));
    }

    public void repair(RepairMan repairMan, int repairedAmount) {
        if (!wearLevel.isBroken()) {
            System.out.println("repairing repaired machine, wtf???" + toString());
            return;
        }

        wearLevel.repairDamage(repairedAmount);

        if (!wearLevel.isBroken()) {
            int brokenTime = SmartFactory.getInstance().getCurrentTakt() - brokenAt;
            Outage outage = Outage.builder()
                    .fromTakt(brokenAt)
                    .lenght(brokenTime)
                    .build();
            outages.add(outage);

            notifyObservers(new RepairedEvent(this, repairMan, brokenTime));
        }

    }

    public WearLevel getWearLevel() {
        return wearLevel;
    }

    public boolean isBroken() {
        return wearLevel.isBroken();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "Machine{" +
                "id=" + id +
                ", priority=" + priority +
                ", broken=" + isBroken() +
                ", wearLevel=" + getWearLevel() +
                '}';
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Set<Outage> getOutages() {
        return outages;
    }
}
