package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.entity.product.Series;

public class SeriesFactory {

    private static int id = 0;
    public static final int SERIES_SIZE = 10;

    public static Series createLaptopSeries() {
        return new Series(ProductFactory.createLaptop(), SERIES_SIZE);
    }
}
