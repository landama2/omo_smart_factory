package cz.cvut.fel.omo.smartfactory.entity.product;

import java.util.Iterator;

public interface SeriesIterable {

    public Iterator createIterator();
}
