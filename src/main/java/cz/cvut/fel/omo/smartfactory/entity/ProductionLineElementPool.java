package cz.cvut.fel.omo.smartfactory.entity;

import cz.cvut.fel.omo.smartfactory.entity.pools.ObjectPool;
import cz.cvut.fel.omo.smartfactory.objectfactories.MachineFactory;
import cz.cvut.fel.omo.smartfactory.production.Human;
import cz.cvut.fel.omo.smartfactory.production.Machine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLineElement;
import cz.cvut.fel.omo.smartfactory.utils.Utils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ProductionLineElementPool extends ObjectPool<ProductionLineElement> implements IterableItem {

    private static ProductionLineElementPool instance = null;
    private int globalId = 0;

    private ProductionLineElementPool() {
    }

    public static ProductionLineElementPool getInstance() {
        if (instance == null) {
            instance = new ProductionLineElementPool();
        }
        return instance;
    }

    /**
     * Get all free productionLineElements that provide required material.
     * @param material required material
     * @return list of available elements
     */
    public List<ProductionLineElement> getFreeList(Material material) {
        return this.availableStream()
                .filter(el -> el.getProvidedMaterial() == material)
                .collect(Collectors.toList());
    }

    void iterateMachines(Consumer<Machine> consumer) {
        objects.keySet()
                .stream()
                .filter(el -> el instanceof Machine)
                .map(el -> (Machine) el)
                .sorted(Comparator.comparing(Machine::getWearLevel).reversed())
                .forEach(consumer);
    }

    void generateRandomLine(int amount) {
        if (objects.isEmpty()) {
            for (int i = 0; i < amount; i++) {
                objects.put(new Human(Utils.chooseRandomMaterial(), 3, globalId++), true);
            }
            objects.put(new Human(Material.CHARGER, 3, globalId++), true);
            objects.put(new Human(Material.HEADPHONES, 3, globalId++), true);
            objects.put(MachineFactory.createBatteryAdder(), true);
            objects.put(MachineFactory.createButtonAdder(), true);
            objects.put(MachineFactory.createCameraAdder(), true);
            objects.put(MachineFactory.createCaseAdder(), true);
            objects.put(MachineFactory.createCdRomAdder(), true);
            objects.put(MachineFactory.createDisplayAdder(), true);
            objects.put(MachineFactory.createChipAdder(), true);
            objects.put(MachineFactory.createKeyboardAdder(), true);
        }
    }

    void generateLine(int humans, int machines) {
        if (objects.isEmpty()) {
            for (int i = 0; i < humans; i++) {
                objects.put(new Human(Utils.chooseRandomMaterial(), Utils.random(1, 3), globalId++), true);
            }
            objects.put(new Human(Material.CHARGER, 3, globalId++), true);
            objects.put(new Human(Material.HEADPHONES, 3, globalId++), true);

            objects.put(MachineFactory.createBatteryAdder(), true);
            objects.put(MachineFactory.createButtonAdder(), true);
            objects.put(MachineFactory.createCameraAdder(), true);
            objects.put(MachineFactory.createCaseAdder(), true);
            objects.put(MachineFactory.createCdRomAdder(), true);
            objects.put(MachineFactory.createDisplayAdder(), true);
            objects.put(MachineFactory.createChipAdder(), true);
            objects.put(MachineFactory.createKeyboardAdder(), true);
        }
    }

    void clear() {
        this.objects = new HashMap<>();
    }

    @Override
    public void iterate(IteratorConsumer consumer) {
        objects.keySet().forEach(consumer::accept);
    }
}
