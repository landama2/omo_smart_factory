package cz.cvut.fel.omo.smartfactory.event;

import cz.cvut.fel.omo.smartfactory.entity.FactoryObservable;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.reports.EventReport;

public abstract class Event {

    protected final FactoryObservable source;
    protected String handler = "Without assigned handler";
    private int takt;

    public Event(FactoryObservable source) {
        this.source = source;
        EventReport.getInstance().registerEvent(this);
        this.takt = SmartFactory.getCurrTakt();
    }

    public void handle(String handler) {
        this.handler = handler;
    }

    public FactoryObservable getSource() {
        return source;
    }

    public String getHandler() {
        return handler;
    }

    public int getTakt() {
        return takt;
    }
}
