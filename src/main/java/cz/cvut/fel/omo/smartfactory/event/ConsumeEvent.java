package cz.cvut.fel.omo.smartfactory.event;

import cz.cvut.fel.omo.smartfactory.entity.FactoryObservable;

public class ConsumeEvent extends Event {
    public ConsumeEvent(FactoryObservable source) {
        super(source);
    }
}
