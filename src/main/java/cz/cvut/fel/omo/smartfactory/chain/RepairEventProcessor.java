package cz.cvut.fel.omo.smartfactory.chain;

import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.event.RepairedEvent;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLineElement;

public class RepairEventProcessor implements Chain {

    private Chain nextInChain;
    private ProductionLine productionLine;

    public RepairEventProcessor(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public void setNextChain(Chain nextChain) {
        nextInChain = nextChain;
    }

    @Override
    public void process(Event event) {
        if (event instanceof RepairedEvent) {
            notifyRepaired((RepairedEvent) event);
        } else {
            nextInChain.process(event);
        }
    }

    private void notifyRepaired(RepairedEvent event) {
        for (ProductionLineElement productionLineElement : productionLine.getElements()) {
            if (productionLineElement == event.getMachine()) {
                productionLine.setOperational(true);
                break;
            }
        }
    }
}
