package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class ChargerAdder extends Machine {

    public ChargerAdder(int id) {
        super(Material.CHARGER, 1, 1, 1, id);
    }
}
