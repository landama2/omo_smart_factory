package cz.cvut.fel.omo.smartfactory.entity;

import cz.cvut.fel.omo.smartfactory.entity.employees.Director;
import cz.cvut.fel.omo.smartfactory.entity.employees.Inspector;
import cz.cvut.fel.omo.smartfactory.entity.pools.RepairManPool;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;
import cz.cvut.fel.omo.smartfactory.reports.ConsumptionReport;
import cz.cvut.fel.omo.smartfactory.reports.EventReport;
import cz.cvut.fel.omo.smartfactory.objectfactories.ProductFactory;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLineManagement;
import cz.cvut.fel.omo.smartfactory.reports.OutagesReport;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class SmartFactoryController {

    private static SmartFactoryController instance = null;
    private EventReport eventReport;
    private SmartFactory factory;
    private ProductionLineElementPool pool = ProductionLineElementPool.getInstance();
    private RepairManPool repairManPool;
    private ProductionLineManagement productionLineManagement = ProductionLineManagement.getInstance();
    private Queue<Product> productsToMake = new LinkedList<>();
    private int SLEEP_TIME = 1;
    private int DIRECTOR_VISIT_PERIOD = 50;
    private int INSPECTION_PERIOD = 100;

    private SmartFactoryController() {
    }

    /**
     *
     * @return instance of SmartFactoryController
     */
    public static SmartFactoryController getInstance() {
        if (instance == null) {
            instance = new SmartFactoryController();
        }
        return instance;
    }

    /**
     * Creates randomly configurated Smart factory with specified production lines
     * @param phoneProdLines damageAmount of phone production lines to create
     * @param laptopProdLines damageAmount of laptop production lines to create
     */
    public void createRandomFactory(int phoneProdLines, int laptopProdLines) {
        factory = SmartFactory.getInstance();
        pool.generateRandomLine(phoneProdLines*5+laptopProdLines*8);
        repairManPool = RepairManPool.getInstance();
        repairManPool.createRepairmen();
        pool.iterateMachines(machine -> machine.addObserver(repairManPool));

        for (int i = 0; i < phoneProdLines; i++) {
            assembleLine(ProductFactory.createSmartPhone());
        }
        for (int i = 0; i < laptopProdLines; i++) {
            assembleLine(ProductFactory.createLaptop());
        }

        System.out.println("Random factory was created!");
    }

    /**
     * Creates configured Smart factory with specified production lines and exact amounts of elements
     * @param humans
     * @param machines
     * @param phoneProdLines
     * @param laptopProdLines
     * @param repairmen
     */
    public void createFactory(int humans, int machines, int phoneProdLines, int laptopProdLines, int repairmen) {
        this.productsToMake.add(ProductFactory.createSmartPhoneWithHeadPhonesForSeries());
        this.productsToMake.add(ProductFactory.createLaptop());
        this.productsToMake.add(ProductFactory.createLaptop());
        this.productsToMake.add(ProductFactory.createSmartPhone());

        this.factory = SmartFactory.getInstance();
        this.pool = ProductionLineElementPool.getInstance();
        pool.generateLine(humans, machines);
        this.repairManPool = RepairManPool.getInstance();
        repairManPool.createRepairmen(repairmen);
        pool.iterateMachines(machine -> machine.addObserver(repairManPool));

        for (int i = 0; i < phoneProdLines; i++) {
            assembleLine(ProductFactory.createSmartPhone());
        }
        for (int i = 0; i < laptopProdLines; i++) {
            assembleLine(ProductFactory.createLaptop());
        }
    }

    private void assembleLine(Product product) {
        productionLineManagement.assembleProductionLine(product);
    }

    /**
     * @return next product to make
     */
    public Product getNextProduct() {
        return productsToMake.poll();
    }

    public void createFactory(Settings settings) {
        createFactory(settings.getHumans(), settings.getMachines(), settings.getPhoneProdLines(), settings.getLaptopProdLines(), settings.getRepairmen());
    }

    /**
     * Assembles phone production line if it is possible
     * @param amount
     */
    public void addPhoneProductionLines(int amount) {
        for (int i = 0; i < amount; i++) {
            assembleLine(ProductFactory.createSmartPhone());
        }
    }

    /**
     * Assembles laptop production line if it is possible
     * @param amount
     */
    public void addLaptopProductionLines(int amount) {
        for (int i = 0; i < amount; i++) {
            productionLineManagement.assembleProductionLine(ProductFactory.createLaptop());
        }
    }

    /**
     * Simulates factory for amount of takts
     * @param takts for takts
     */
    public void simulate(int takts) {
        for (int i = 0; i < takts; i++) {
            factory.simulate();

            if (factory.getCurrentTakt() % DIRECTOR_VISIT_PERIOD == 0) {
                System.out.println("====== Director visit ======");
                Director director = new Director();
                factory.acceptDirector(director);
                System.out.println("============================");
            }
            if (factory.getCurrentTakt() % INSPECTION_PERIOD == 0) {
                System.out.println("====== Inspector visit ======");
                Inspector inspector = new Inspector();
                pool.iterateMachines(inspector::accept);
                System.out.println("=============================");
            }

            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("For " + takts + " takts production lines made:");
        for (ProductionLine productionLine : factory.getProductionLines()) {
            if(productionLine != null) {
                if (productionLine.getActiveSeries() != null) {
                    System.out.println("Production line " + productionLine.getId() + " made " + productionLine.getActiveSeries().getAmount() + " products.");
                }
            } else {
                System.out.println("Factory doesn´t have any production lines!");
            }
        }
        System.out.println("\n");
    }

    /**
     * Clears factory settings
     */
    public void clear() {
        factory.clear();
        pool.clear();
        EventReport.getInstance().clear();
    }

    /**
     * Prints event report
     */
    public void printEvents() {
        eventReport = EventReport.getInstance();
        eventReport.printReportToSout();
        eventReport.printReportToFile();
        System.out.println("\n");
    }

    /**
     * Prints event report in interval
     */
    public void printEventsInInterval(int from, int to) {
        eventReport = EventReport.getInstance();
        eventReport.printReportToFileInRange(from, to);
        System.out.println("\n");
    }

    /**
     * Prints consumption report
     */
    public void printConsumptionReport() {
        ConsumptionReport consumptionReport = new ConsumptionReport();
        consumptionReport.printReportToSout();
        consumptionReport.printReportToFile();
        System.out.println("\n");
    }

    /**
     * Prints consumption report in interval
     */
    public void printConsumptionReportInInterval(int from, int to) {
        ConsumptionReport consumptionReport = new ConsumptionReport();
        consumptionReport.printReportToFileInRange(from, to);
        System.out.println("\n");
    }

    /**
     * Prints outages report
     */
    public void printOutagesReport() {
        OutagesReport outagesReport = new OutagesReport();
        outagesReport.generateReport(factory);
        outagesReport.printReport();
        outagesReport.printReportToFile();
        System.out.println("\n");
    }

    /**
     * Prints outages in interval
     */
    public void printOutagesReportInInterval(int from, int to) {
        OutagesReport outagesReport = new OutagesReport();
        outagesReport.generateReport(factory);
        outagesReport.printReportToFileInRange(from, to);
    }

    /**
     * Prints amount of finished series
     */
    public void printAmountOfFinishedSeries() {
        System.out.println("Factory has finished " + factory.getAmountOfFinishedSeries() + " products.");
    }

    public List<ProductionLine> getProductionLines() {
        return factory.getProductionLines();
    }

    /**
     * @return Amount of production lines
     */
    public int getProductionLinesAmount() {
        return factory.getProductionLines().size();
    }

    /**
     * @return Amount of finished series
     */
    public int getAmountOfFinishedSeries() {
        return factory.getAmountOfFinishedSeries();
    }

}
