package cz.cvut.fel.omo.smartfactory.utils;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.*;

public class Utils {

    private static final List<Material> VALUES =
            Collections.unmodifiableList(Arrays.asList(Material.values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static Material chooseRandomMaterial() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    public static int random(int min, int max) {
        double random = new Random().nextDouble();
        return (int) (min + (random * (max - min)));
    }

}
