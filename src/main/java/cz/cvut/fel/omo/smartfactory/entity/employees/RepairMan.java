package cz.cvut.fel.omo.smartfactory.entity.employees;

import cz.cvut.fel.omo.smartfactory.event.BreakEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.event.EventHandler;
import cz.cvut.fel.omo.smartfactory.production.Machine;
import cz.cvut.fel.omo.smartfactory.production.MachineInfo;

import java.util.Random;

public class RepairMan implements EventHandler {

    private int id;
    private BreakEvent dispatchedEvent;
    private Machine assignedToFix;
    private MachineInfo info;

    public RepairMan(int id) {
        this.id = id;
    }

    public void dispatch(BreakEvent event) {
//    public void dispatch(Machine machine) {
        if (event.getMachine() != null) {
            this.assignedToFix = event.getMachine();
            this.info = (MachineInfo) assignedToFix.getInfo();
            System.out.println("Repairman " + id + " was dispatched to repair machine " + assignedToFix.getId() + ".");
        }
    }

    public void release() {
        this.assignedToFix = null;
        System.out.println("Repairman " + id + " is now available.");
    }

    public void simulate() {
        if (assignedToFix != null) {
            assignedToFix.repair(this, new Random().nextInt(35));
        }
    }

    public Machine getAssignedToFix() {
        return this.assignedToFix;
    }

    @Override
    public String toString() {
        return "RepairMan{" +
                "id=" + id +
//                ", dispatchedEvent=" + dispatchedEvent +
//                ", assignedToFix=" + assignedToFix +
//                ", info=" + info +
                '}';
    }

    @Override
    public void handleEvent(Event event) {
        if (event instanceof BreakEvent) {
            BreakEvent breakEvent = (BreakEvent) event;
            if (breakEvent.getMachine() != null) {
                this.assignedToFix = breakEvent.getMachine();
                this.info = (MachineInfo) assignedToFix.getInfo();
                System.out.println("Repairman " + id + " was assigned to repair machine " + assignedToFix.getId() + ".");
            }
        }

    }
}
