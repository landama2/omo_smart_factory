package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class ButtonAdder extends Machine {

    public ButtonAdder(int id) {
        super(Material.BUTTON, 1, 1, 1, id);
    }
}
