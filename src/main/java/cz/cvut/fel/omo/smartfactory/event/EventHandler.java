package cz.cvut.fel.omo.smartfactory.event;

public interface EventHandler {

    void handleEvent(Event event);
}
