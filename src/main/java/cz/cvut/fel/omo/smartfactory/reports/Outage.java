package cz.cvut.fel.omo.smartfactory.reports;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Outage {

    private Integer fromTakt;
    private Integer lenght;

}
