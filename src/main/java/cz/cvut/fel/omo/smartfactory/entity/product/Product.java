package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.Queue;

public abstract class Product {

    private ProductState state = ProductState.NONE;

    Queue<Material> requiredMaterials;

    int id;

    public int getId() {
        return id;
    }

    public abstract String getDescription();

    public ProductState getState() {
        return state;
    }

    private void complete() {
        state = ProductState.COMPLETED;
    }

    public void workOn() {
        state = ProductState.IN_PROGRESS;
        if (requiredMaterials.isEmpty()) {
            complete();
        }
    }

    public void plan() {
        state = ProductState.PLANNED;
    }

    public Queue<Material> getRequiredMaterials() {
        return requiredMaterials;
    }

    public boolean isCompleted() {
        return state == ProductState.COMPLETED;
    }

    @Override
    public String toString() {
        return "Product{" +
                "state=" + state +
                ", requiredMaterials=" + requiredMaterials +
                ", id=" + id +
                '}';
    }
}
