package cz.cvut.fel.omo.smartfactory.entity;

import cz.cvut.fel.omo.smartfactory.entity.product.Series;
import cz.cvut.fel.omo.smartfactory.production.Machine;
import cz.cvut.fel.omo.smartfactory.production.ProductionLine;

public interface Visitor {

    public void visit(Machine machine);
    public void visit(SmartFactory factory);
    public void visit(ProductionLine line);
    public void visit(Series series);
}
