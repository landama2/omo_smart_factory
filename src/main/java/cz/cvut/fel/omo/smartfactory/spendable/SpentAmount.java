package cz.cvut.fel.omo.smartfactory.spendable;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SpentAmount {

    private int amount;
    private int takt;
}
