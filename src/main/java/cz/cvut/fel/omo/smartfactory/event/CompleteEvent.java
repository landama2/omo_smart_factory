package cz.cvut.fel.omo.smartfactory.event;

import cz.cvut.fel.omo.smartfactory.production.ProductionLineElement;

public class CompleteEvent extends Event {

    public CompleteEvent(ProductionLineElement element) {
        super(element);
    }

    public ProductionLineElement getElement() {
        return (ProductionLineElement) source;
    }
}
