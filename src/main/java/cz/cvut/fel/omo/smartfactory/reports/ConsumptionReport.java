package cz.cvut.fel.omo.smartfactory.reports;

import cz.cvut.fel.omo.smartfactory.entity.IterableItem;
import cz.cvut.fel.omo.smartfactory.entity.IteratorConsumer;
import cz.cvut.fel.omo.smartfactory.entity.ProductionLineElementPool;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactory;
import cz.cvut.fel.omo.smartfactory.production.Human;
import cz.cvut.fel.omo.smartfactory.production.HumanInfo;
import cz.cvut.fel.omo.smartfactory.production.Machine;
import cz.cvut.fel.omo.smartfactory.production.MachineInfo;
import cz.cvut.fel.omo.smartfactory.spendable.Energy;
import cz.cvut.fel.omo.smartfactory.spendable.Money;
import cz.cvut.fel.omo.smartfactory.spendable.Oil;

import java.util.ArrayList;
import java.util.List;

public class ConsumptionReport extends Reporter implements IteratorConsumer {

    public static final String CONSUMPTION_REPORT_TXT = "consumption_report.txt";
    private Money totalCost = new Money();
    private Energy totalEnergyConsumption = new Energy();
    private Oil totalOilComsumption = new Oil();

    public ConsumptionReport() {
        super(CONSUMPTION_REPORT_TXT);
        ProductionLineElementPool.getInstance().iterate(this::accept);
    }

    @Override
    public void accept(IterableItem item) {
        if (item instanceof Human) {
            this.totalCost.addAmount(((HumanInfo) ((Human) item).getInfo()).getMoneyCost());
        } else if (item instanceof Machine) {
            this.totalEnergyConsumption.addAmount(((MachineInfo) ((Machine) item).getInfo()).getTotalEnergyConsumption());
            this.totalOilComsumption.addAmount(((MachineInfo) ((Machine) item).getInfo()).getTotalOilConsumption());
        }
    }

    @Override
    public String createReport() {
        List<String> lines = new ArrayList<>();
        lines.add("====== Consumption report (" + SmartFactory.getCurrTakt() + " taktss) ======");
        lines.add("Total cost of human elements: " + totalCost.getAmount());
        lines.add("Total energy consumption of machines: " + totalEnergyConsumption.getAmount());
        lines.add("Total oil consumption of machines: " + totalOilComsumption.getAmount());
        return String.join("\n", lines);
    }

    @Override
    public String createReportInRange(int from, int to) {
        List<String> lines = new ArrayList<>();
        if (coversAll(from, to)) {
            lines.add("====== Consumption report (" + SmartFactory.getCurrTakt() + " taktss) ======");
        } else {
            lines.add("====== Consumption report (" + from + " -> " + to + ") ======");
        }
        lines.add("Total cost of human elements: " + totalCost.getAmountInRange(from, to));
        lines.add("Total energy consumption of machines: " + totalEnergyConsumption.getAmountInRange(from, to));
        lines.add("Total oil consumption of machines: " + totalOilComsumption.getAmountInRange(from, to));
        return String.join("\n", lines);
    }


}
