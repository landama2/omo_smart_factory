package cz.cvut.fel.omo.smartfactory.objectfactories;

import cz.cvut.fel.omo.smartfactory.production.ProductionLine;

public class ProductionLineFactory {

    private static int id = 0;

    public static ProductionLine createProductionLine() {
        return new ProductionLine(id++);
    }
}
