package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.spendable.Consumption;

abstract class Info {

    abstract void increment(Consumption consumption);

    abstract Info copy();
}
