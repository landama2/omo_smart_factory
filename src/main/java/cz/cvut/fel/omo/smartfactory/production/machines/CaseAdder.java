package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class CaseAdder extends Machine {

    public CaseAdder(int id) {
        super(Material.CASE, 5, 2, 3, id);
    }
}
