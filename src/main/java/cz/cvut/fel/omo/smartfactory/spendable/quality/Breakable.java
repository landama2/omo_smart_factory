package cz.cvut.fel.omo.smartfactory.spendable.quality;

public interface Breakable {

    int getDamageAmount();

    void damage(int amount);
}
