package cz.cvut.fel.omo.smartfactory.entity;

public interface IterableItem {

    void iterate(IteratorConsumer consumer);
}
