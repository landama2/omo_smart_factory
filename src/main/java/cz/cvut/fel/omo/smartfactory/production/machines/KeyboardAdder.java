package cz.cvut.fel.omo.smartfactory.production.machines;

import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.production.Machine;

public class KeyboardAdder extends Machine {

    public KeyboardAdder(int id) {
        super(Material.KEYBOARD, 3, 1, 2, id);
    }
}
