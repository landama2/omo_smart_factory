package cz.cvut.fel.omo.smartfactory.entity.product;

public enum ProductState {

    NONE("Product is not planned for production"),
    PLANNED("Product is planned for production in a series"),
    IN_PROGRESS("Product is being produced"),
    COMPLETED("Product is completed");

    private String description;

    ProductState(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public ProductState nextState() {
        switch (this) {
            case NONE:
                return PLANNED;
            case PLANNED:
                return IN_PROGRESS;
            case IN_PROGRESS:
                return COMPLETED;
            case COMPLETED:
                return COMPLETED;
            default: return NONE;
        }
    }
}

