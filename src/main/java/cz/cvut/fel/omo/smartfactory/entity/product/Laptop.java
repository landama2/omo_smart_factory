package cz.cvut.fel.omo.smartfactory.entity.product;

import cz.cvut.fel.omo.smartfactory.entity.Material;

import java.util.LinkedList;

public class Laptop extends Product {

    public Laptop(int id) {
        this.id = id;
        this.requiredMaterials = new LinkedList<>();
        addRequiredMaterial();
    }

    private void addRequiredMaterial() {
        requiredMaterials.add(Material.CASE);
        requiredMaterials.add(Material.BATTERY);
        requiredMaterials.add(Material.BATTERY);
        requiredMaterials.add(Material.CHIP);
        requiredMaterials.add(Material.KEYBOARD);
        requiredMaterials.add(Material.CD_ROM);
        requiredMaterials.add(Material.DISPLAY);
        requiredMaterials.add(Material.CASE);
    }

    @Override
    public String getDescription() {
        return "Laptop";
    }
}
