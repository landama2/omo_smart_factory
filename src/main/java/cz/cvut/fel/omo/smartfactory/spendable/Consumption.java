package cz.cvut.fel.omo.smartfactory.spendable;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Consumption {

    private int energyConsumption;
    private int oilConsumption;
    private int wearFactor;
    private int money;

}
