package cz.cvut.fel.omo.smartfactory.entity.pools;

import cz.cvut.fel.omo.smartfactory.entity.ElementObserver;
import cz.cvut.fel.omo.smartfactory.entity.employees.RepairMan;
import cz.cvut.fel.omo.smartfactory.event.BreakEvent;
import cz.cvut.fel.omo.smartfactory.event.Event;
import cz.cvut.fel.omo.smartfactory.event.RepairedEvent;
import cz.cvut.fel.omo.smartfactory.objectfactories.RepairManFactory;
import cz.cvut.fel.omo.smartfactory.utils.Utils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Queue;

public class RepairManPool extends ObjectPool<RepairMan> implements ElementObserver {

    private static final int RANDOM_REPAIRMAN_COUNT = Utils.random(1, 3);
    private static RepairManPool instance;

    private Queue<BreakEvent> brokenMachineEvents = new PriorityQueue<>(
            Comparator.comparing((BreakEvent event)-> event.getMachine().getPriority())
            .thenComparing(Event::getTakt)
    );

    private RepairManPool() {
    }

    public static RepairManPool getInstance() {
        if (instance == null) {
            instance = new RepairManPool();
        }
        return instance;
    }

    public void createRepairmen() {
        for (int i = 0; i < RANDOM_REPAIRMAN_COUNT; i++) {
            this.objects.put(RepairManFactory.createRepairMan(), true);
        }
    }

    public void createRepairmen(int amount) {
        for (int i = 0; i < amount; i++) {
            this.objects.put(RepairManFactory.createRepairMan(), true);
        }
    }

    public void clear() {
        this.objects = new HashMap<>();
        brokenMachineEvents = new PriorityQueue<>(
                (e1, e2) -> Integer.compare(e2.getMachine().getPriority(), e1.getMachine().getPriority())
        );
    }

    @Override
    public void update(Event event) {
        if (event instanceof BreakEvent) {
            handleBreakEvent((BreakEvent) event);
        } else if (event instanceof RepairedEvent) {
            handleRepairedEvent((RepairedEvent) event);
        }
    }

    private void handleBreakEvent(BreakEvent event) {
        brokenMachineEvents.add(event);
        assignRepairman();
    }

    private void handleRepairedEvent(RepairedEvent event) {
        RepairMan repairMan = event.getRepairMan();
        repairMan.release();
        makeAvailable(repairMan);
        assignRepairman();
    }

    private void assignRepairman() {
        while (!brokenMachineEvents.isEmpty()) {
            RepairMan repairMan = this.pollAvailable();
            if (repairMan == null) {
                System.out.println("No repairman is available at this moment.");
                return;
            }

            BreakEvent event = brokenMachineEvents.poll();
            repairMan.handleEvent(event);
        }
    }

    public void simulate() {
        objects.keySet().forEach(RepairMan::simulate);
    }

}
