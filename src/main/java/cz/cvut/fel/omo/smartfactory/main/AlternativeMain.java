package cz.cvut.fel.omo.smartfactory.main;

import cz.cvut.fel.omo.smartfactory.entity.Settings;
import cz.cvut.fel.omo.smartfactory.entity.SmartFactoryController;

public class AlternativeMain {
    public static void main(String[] args) {

        Settings settings2 = Settings.builder()
                .humans(10)
                .laptopProdLines(1)
                .phoneProdLines(1)
                .repairmen(10)
                .machines(5)
                .build();
        settings2.storeToJson("settings2.json");

        Settings settingsFromFile2 = Settings.loadFromJson("settings2.json");

        SmartFactoryController controller = SmartFactoryController.getInstance();
        controller.createFactory(settingsFromFile2);
        controller.simulate(2500);
        controller.printConsumptionReport();
        controller.printConsumptionReportInInterval(20, 50);
        controller.printEventsInInterval(20, 50);
        controller.printEvents();
        controller.printOutagesReportInInterval(20, 50);
        controller.printOutagesReport();
        controller.printAmountOfFinishedSeries();
        controller.clear();
    }
}
