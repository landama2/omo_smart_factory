package cz.cvut.fel.omo.smartfactory.production;

import cz.cvut.fel.omo.smartfactory.entity.FactoryObservable;
import cz.cvut.fel.omo.smartfactory.entity.IterableItem;
import cz.cvut.fel.omo.smartfactory.entity.Material;
import cz.cvut.fel.omo.smartfactory.entity.product.Product;

public interface ProductionLineElement extends FactoryObservable, IterableItem {

    Material getProvidedMaterial();

    void giveMaterial(Material material);

    Info getInfo();

    void acceptProduct(Product product);

    void setNext(ProductionLineElement next);

    void simulate();

    Product workOnProduct();
}
