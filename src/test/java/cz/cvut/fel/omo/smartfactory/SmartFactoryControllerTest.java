package cz.cvut.fel.omo.smartfactory;

import cz.cvut.fel.omo.smartfactory.entity.SmartFactoryController;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SmartFactoryControllerTest {

    private SmartFactoryController controller = SmartFactoryController.getInstance();

    @After
    public void clear() {
        controller.clear();
    }

    @Test
    public void createRandomFactory_empty() {
        controller.createRandomFactory(0,0);
        assertEquals(0, controller.getProductionLinesAmount());
    }

    @Test
    public void createRandomFactory_nonempty() {
        controller.createRandomFactory(1,0);
        assertEquals(1, controller.getProductionLinesAmount());
    }

    @Test
    public void createFactory_empty() {
        controller.createFactory(0,0,0,0,0);
        assertEquals(0, controller.getProductionLinesAmount());
    }

    @Test
    public void createFactory_nonempty() {
        controller.createFactory(5,3,1,0,1);
        assertEquals(1, controller.getProductionLinesAmount());
    }


    @Test
    public void simulate_empty_noproducts() {
        controller.createRandomFactory(0, 0);
        controller.simulate(100);
        assertEquals(0, controller.getAmountOfFinishedSeries());
    }

    @Test
    public void simulate_nonempty_products() {
        controller.createRandomFactory(1, 1);
        controller.simulate(100);
        assertNotEquals(0, controller.getAmountOfFinishedSeries());
    }
}
