package cz.cvut.fel.omo.smartfactory.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class SettingsTest {

    @Test
    public void store_and_load() {
        Settings settings = Settings.builder()
                .humans(10)
                .laptopProdLines(1)
                .phoneProdLines(1)
                .repairmen(5)
                .machines(10)
                .build();
        settings.storeToJson("settings_test.json");

        Settings settingsFromFile = Settings.loadFromJson("settings_test.json");
        assertEquals(settings, settingsFromFile);
    }

}