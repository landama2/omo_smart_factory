package cz.cvut.fel.omo.smartfactory.spendable;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpendableImplTest {

    @Test
    public void addAmount_spendable() {
        Energy energy = new Energy();
        energy.addAmount(10);
        Energy energy2 = new Energy();
        energy2.addAmount(30);
        energy.addAmount(energy2.getAmount());
        assertEquals(energy.getAmount(), 40);
    }

    @Test
    public void addAmount_numbers() {
        Energy energy = new Energy();
        energy.addAmount(10);
        energy.addAmount(20);
        assertEquals(energy.getAmount(), 30);
    }
}