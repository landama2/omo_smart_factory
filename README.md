Smartfactory

Jedna se o SmartFactory vyrabejici telefony a laptopy. Stroje, lide a roboti mohou poskytovat 
ruzne materialy a soucastky pro vyrobek(produkt). Stroje maji ruznou spotrebuju a miru opotrebni. 
Po prekroceni miry opotrebeni se vygeneruje event, ktery zpracuji dostupni opravari a pote postupne
 opravuji rozbity stroj.
Produkty jsou vyrabeny v seriich po x kusech, pote se zacne vyrabet dalsi produkt a 
linka se pripadne presklada.
Zaznamy o spotrebe a eventech se ukladaji, nakonci je z nich mozno vytvorit reporty(celkove nebo za casove obdobi).


Design patterny:
Singleton – SmartFactory
State Machine - ProductState
Iterator – interface IterableItem, IteratorConsumer
Visitor, Observer – interface FactoryObservable, FactoryObserver - eventy dejici se na lince
Chain of responsibility – Chain - procesovani Eventu
Object Pool – ProductionLineElementPool, RepairManPool
Decorator - HeadPhones, Charger, rozsiruji normalni produkty o tyto subprodukty
Lazy Initialization – metody getInstance -> vytvari instance az kdyz se poprve pozaduje

Mapovani funkcnich pozadavku na kod:
Funkční požadavky
 F1 Hlavní entity se kterými pracujeme je továrna(=SmartFactory), linka(=ProductionLine) (s prioritou), stroj(=Machine), člověk(=Human) a výrobek(=Product), materiál(=Material) plus libovolné další entity. Stroje, lidé i výrobky mohou být různého druhu
F2.Produkty se vyrábějí v sériích po několika stech kusů, jestliže se mění série nekompatibilních výrobků, tak je potřeba výrobní linky přeskládat. Každý výrobek má definovanou sekvenci zařízení, robotů, lidí, které je potřeba za sebou uspořádat na linku. 
=Series, ProductionLine->ActiveSeries, Stroje a Lide providuji pozadavany material, z nich se sestavi linka, ta pak dela serii vyrobku
F3.Stroje a roboty mají svoji spotřebu; lidé, roboty, stroje i materiál stojí náklady. 
=Consumption, Oil, Energy, Money
F4.Komunikace mezi stroji, roboty a lidmi probíhá pomocí eventů. Event může dostat 1 až N entit (člověk, stroj, robot), které jsou na daný druh eventu zaregistrované. Eventy je potřeba odbavit. 
=Event, ConsumeEvent, BreakEvent, RepairEvent, CompleteEvent a k tomu ElementObserver, eventy se odbavuji pomoci ChainOfResponsibility
F5.Jednotlivá zařízení mají API na sběr dat o tomto zařízení. O zařízeních sbíráme data jako je spotřeba elektřiny, oleje, materiálu a funkčnost (opotřebení roste s časem). 
=getConsumption, getWearLevel(opotrebeni)
F6.Stroje a roboty se po určité době rozbijí. Po rozbití vygenerují event (alert) s prioritou podle důležitosti linky, který odbaví člověk - opravář. 
=WearLevel(stav opotrebeni) -> BreakEvent, RepairEvent - zpracovava RepairMan
F7.Opravářů je omezený počet. Oprava trvá několik taktů. Při začátku opravy a konci opravy je generován event (bude se hodit pro požadavek F10 :-). Vznikají situace, kdy nejsou dostupní žádní opraváři - pak se čeká dokud se některý z nich neuvolní. Po uvolnění opravář nastupuje na 1. nejprioritnější, 2. nejstarší defekt
=BreakEvent, RepairEvent, RepairManPool obsahuje priority queue, z te si bere nezpracovane BreakEventy
F8.Návštěva ředitele a inspektora. Realizujeme návštěvu továrny, kdy ředitel prochází továrnou přesně podle stromové hierarchie entit továrna ->* linka -> *(stroj|robot|člověk nebo výrobek) a inspektor prochází podle míry opotřebení. Ředitel i inspektor mají na sobě definované akce, které provedou s daným typem entity. Zapište sekvenci procházení a názvy provedených akcí do logu. 
=Director, Inspector, pomoci patternu Visitor
F9.Za továrnu je nutné vygenerovat následující reporty za libovolné časové období: ○ FactoryConfigurationReport : veškerá konfigurační data továrny zachovávající hierarchii - továrna ->* linka -> *(stroj|robot|člověk nebo výrobek). ○ EventReport : jaké za jednotlivé období vznikly eventy, kde je grupujeme 1) podle typu eventu, (2) podle typu zdroje eventu a (3) podle toho, kdo je odbavil. ○ ConsumptionReport : Kolik jednotlivé zařízení, roboty spotřebovaly elektřiny, oleje, materiálu. Včetně finančního vyčíslení. V reportu musí být i summární spotřeby za nadřazané entity (linka|továrna) ○ OuttagesReport : Nejdelší výpadek, nejkratší výpadek, průměrná doba výpadku, průměrná doba čekání na opraváře a typy zdrojů výpadků setříděné podle délky výpadku. 
FactoryConfigurationReport - chybi EventReport=EventReport - vypise 1) podle typu eventu, (2) podle typu zdroje eventu a (3) podle toho, kdo je odbavi, zvlada interval 
ConsumptionReport=ConsumptionReport, zvlada interval
 OuttagesReport=OuttagesReport, mel by splnovat vse
F10. Vraťte stavy strojů v zadaném taktu (jiném než posledním :-)). Stav zrekonstruujte z počátečního stavu a sekvence eventů, které byly na stroji provedeny.
= neni naimplementovan

Nefunkční požadavky: Reporty jsou generovány do textového souboru(outages_report.txt, consumption_report.txt, event_report.txt)
 Dvě různé konfigurace továrny - Main, AlternativeMain, konfigurace je mozno nacist ze souboru(settings.json)
